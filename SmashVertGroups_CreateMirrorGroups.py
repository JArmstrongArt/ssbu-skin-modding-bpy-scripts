import bpy

#This script, after using 'SmashVertGroups_MirrorRename.py', will make empty mirror duplicates of all SSBU vertex groups determined to be mirrorable
#You'll generally want to run this once you're done weighting and of course once you have translated the vertex group names with the aforementioned script
#After that, make sure to apply your mirror modifier before exporting, preferrably on a copy of your mesh for safety and convenience

affectOBJs = bpy.context.selected_objects



if affectOBJs is not None and len(affectOBJs)>0:

    for obj in affectOBJs:
        if obj is not None:
            newGroups=[]
            for vGroup in obj.vertex_groups:
                if vGroup is not None:
                    vName = vGroup.name
                    lastChar_index = len(vName)-1
                    dotChar_index = len(vName)-2
                    
                    lastChar = vName[lastChar_index]
                    dotChar = vName[dotChar_index]
                    
                    if(lastChar=='R' or lastChar=='L'):
                        if(dotChar=='.'):
                            
                            vName_p1 = vName[0:dotChar_index]
                            
                            vName_p2=""
                            if(lastChar=='R'):
                                vName_p2 = 'L'
                            
                            if(lastChar=='L'):
                                vName_p2='R'
                            
                            newGroups.append(vName_p1+"."+vName_p2)
                            
            if newGroups is not None and len(newGroups)>0:
                for groupName in newGroups:
                    if groupName is not None:
                        groupExists=False
                        
                        for vGroup in obj.vertex_groups:
                            if vGroup is not None:
                                if vGroup.name == groupName:
                                    groupExists=True
                                    break
                            
                        if groupExists==False:

                            obj.vertex_groups.new( name = groupName )
            
