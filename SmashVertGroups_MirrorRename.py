import bpy

#False = Rename all groups to work with Blender's Mirror modifier
#True = Reverse the effects of running this script in reverseMode=False for exporting to SSBU (make sure to apply your Mirror modifier beforehand!)
reverseMode=False

#for any vert group name that has numbers and letters following the 'R' or 'L' which is normally at the end of the vert group name, please enter every letter before 'R' or 'L' as a word below (e.g. FingerR55 = 'Finger', DoorL42_bees = 'Door')
#For a lot of characters, leaving this list at just 'Finger' will be enough
postCheckPrefixes = ['Finger','Toe']








#ignore
affectOBJs = bpy.context.selected_objects
suffixBank = ['R','L']

if affectOBJs is not None and len(affectOBJs)>0 and suffixBank is not None and len(suffixBank)>0:
    for obj in affectOBJs:
        if obj is not None:
            for vGroup in obj.vertex_groups:
                if vGroup is not None:
                    alreadyRenamed=False
                    vName = vGroup.name
                    
                    if vName is not None and len(vName)>0:
                        
                        for suf in suffixBank:
                            alreadyRenamed_sufFind = vName.find(suf)
                            if alreadyRenamed_sufFind>=0 and alreadyRenamed_sufFind>=len(vName)-1:
                                alreadyRenamed_dotFind = vName.find(".")
                                if alreadyRenamed_dotFind >=0 and alreadyRenamed_dotFind == alreadyRenamed_sufFind-1:
                                    alreadyRenamed=True
                                    break
                        
                        if alreadyRenamed == reverseMode:
                            lastSuffixIndex = None
                            for x in range(0,len(vName)):
                                if vName[x] in suffixBank:
                                    lastSuffixIndex = x
                            
                            if lastSuffixIndex is not None and lastSuffixIndex>=0:
                                prefixUsed=None
                                if postCheckPrefixes is not None and len(postCheckPrefixes)>0:
                                    for prefIndex in range(0, len(postCheckPrefixes)):
                                        if postCheckPrefixes[prefIndex] is not None and postCheckPrefixes[prefIndex] in vName and vName.find(postCheckPrefixes[prefIndex]) ==0:
                                            prefixUsed=prefIndex
                                            break
                                
                                
                                if reverseMode ==False:
                                    newName_p1 = vName[:lastSuffixIndex]
                                    newName_p2 = vName[lastSuffixIndex]
                                    
                                    newName_p3=""
                                    
                                    if prefixUsed is not None and prefixUsed>=0 and lastSuffixIndex<len(vName)-1:
                                        newName_p3 = vName[lastSuffixIndex+1:]
                                    vGroup.name = newName_p1+newName_p3+"."+newName_p2
                                else:
                                    if prefixUsed is not None and prefixUsed>=0:
                                        endOfPrefix = len(postCheckPrefixes[prefixUsed])
                                        newName_p1 = vName[:endOfPrefix]
                                        newName_p2 = vName[len(vName)-1]
                                        newName_p3 = vName[endOfPrefix:len(vName)-2]
                                        vGroup.name = newName_p1 + newName_p2+newName_p3
                                    else:
                                        newName_p1 = vName[lastSuffixIndex]
                                        newName_p2 = vName[:lastSuffixIndex-1]
                                        vGroup.name = newName_p2+newName_p1

                                    
                                    
                            